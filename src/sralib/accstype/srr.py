"""
..
  Copyright 2020 The University of Sydney

  Class implementing the SRA Run accession type SRR [0].SRR is an object containing
  actual sequencing data for a particular sequencing experiment SRX.

  Experiments may contain several Runs depending on the number of sequencing instrument
                 runs that were needed.

  [0]: https://www.ncbi.nlm.nih.gov/books/NBK56913/#search.what_do_the_different_sra_accessi

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

# References:
# [0]: https://www.ncbi.nlm.nih.gov/books/NBK56913/#search.what_do_the_different_sra_accessi
#-------------------------------------------------------------------------------

class Srr:

  def __init__(self):
    self.acc = None
    self.spots = None
    self.bases = None
    self.size = None

  def dump(self):
    return {'acc':self.acc,  'spots':self.spots, 'bases':self.bases, 'size':self.size}
