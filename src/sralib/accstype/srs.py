"""
..
  Copyright 2020 The University of Sydney

  Class implementing the SRA Sample accession type SRS [0].  A Sample is
  an object containing the metadata describing the physical sample upon which a
  sequencing experiment was performed.

  [0]: https://www.ncbi.nlm.nih.gov/books/NBK56913/#search.what_do_the_different_sra_accessi
"""

class SrsSample:

  def __init__(self):
    self.acc = None
    self.name = None
    self.taxid = None
    self.organism = None
    self.sex = None
    self.type = None
