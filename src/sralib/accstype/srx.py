"""
..
  Copyright 2020 The University of Sydney

  Class implementing the SRA Experiment accession type SRX [0].  An Experiment
  is an object containing the metadata for actual sequencing data for a
  particular sequencing experiment SRX. Experiments may contain several
  runs, SRRs and lnked to several SRPs.

  [0]: https://www.ncbi.nlm.nih.gov/books/NBK56913/#search.what_do_the_different_sra_accessi
"""

class Srx:

  class Platform:

    def __init__(self):
      self.name = None
      self.model = None

    def dump(self):
      return {'platform':{ 'model':self.model, 'name':self.name}}

  class Library:

    def __init__(self):
      self.name = None
      self.strategy = None
      self.source = None
      self.selection = None
      self.single = False
      self.paired = None
      self.protocol = None
      self.platform =Srx.Platform()

    def dump(self):
      dump = self.platform.dump()
      dump.update({'library' : {'strategy':self.strategy, 'name':self.name, 'source':self.source,
                    'selection': self.selection, 'single':self.single, 'paired':self.paired}})
      return dump

  class Submitter:

    def __init__(self):
      self.acc = None
      self.center = None
      self.contact = None
      self.lab = None

    def dump(self):
      return {'Submitter': { 'accs':self.acc, 'center':self.center, 'contact':self.contact,
              'lab':self.lab}}

  def __init__(self):
    self.acc = None
    self.name = None
    self.status = None
    self.bioproject = None
    self.biosample = None
    self.spots = 0
    self.bases = 0
    self.size = 0
    self.submitter = Srx.Submitter()
    self.library = Srx.Library()
    self.srrs = {}

  def dump(self):
    dump = {'SRX' : {'acc':self.acc, 'name':self.name, 'bioproject':self.bioproject,
                     'biosample':self.biosample, 'spots':self.spots,
                     'bases':self.bases, 'size':self.size, 'status':self.status}}
    dump.update(self.library.dump())
    dump.update(self.submitter.dump())
    return dump
