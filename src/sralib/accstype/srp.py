"""
..
  Copyright 2020 The University of Sydney
  Class implementing the SRA Study accession type (SRP)[0]. SRP is an object
  containing project metadata describing a sequencing study or project.

  [0]: https://www.ncbi.nlm.nih.gov/books/NBK56913/#search.what_do_the_different_sra_accessi

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""


class Srp:

  def __init__(self):
    self.acc = None
    self.name = None

  def dump(self):
    return {'SRP' :{'accs':self.acc, 'name':self.name}}
