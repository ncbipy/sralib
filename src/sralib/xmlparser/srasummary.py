"""
..
  Copyright 2020 The University of Sydney
  SraRuninfoParser parses NCBI's SRA docsum summaries [0] in XML format.

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import xml.etree.ElementTree


import sralib.accstype.srx
import sralib.accstype.srp
import sralib.accstype.srr
import sralib.accstype.srs

class SraEsummmaryXmlParser:

  class SraEsummary:

    def __init__(self, uid):
      self.uid = int(uid)
      self.srx = sralib.accstype.srx.Srx()
      self.srp = sralib.accstype.srp.Srp()
      self.srs = sralib.accstype.srr.Srr()
      self.srrs = {}

    def dump(self):
      dump = {'uid':self.uid}
      dump.update(self.srx.dump())
      dump.update(self.srp.dump())
      dump.update({'SRR': [ self.srrs[x].dump() for x in self.srrs]})
      return dump

  def __init__(self):
    self.docsums = {}

  def parse(self, docsum):
    uid = None
    status = 0
    context = iter(xml.etree.ElementTree.iterparse(docsum, events=("start", "end")))
    event, root = next(context)
    event, elem = next(context)
    while True:
      #print(status, event, elem.tag)
      if status == 0:
        if event == 'start' and elem.tag == 'Id':
          uid = int(elem.text)
          self.docsums[uid] = SraEsummmaryXmlParser.SraEsummary(elem.text)
        if event == 'end' and elem.tag == 'DocumentSummarySet':
          for i in self.docsums:
            print(self.docsums[i].dump())
          return self.docsums
        if event == 'start' and elem.tag == 'ExpXml':
          status = 1
        if event == 'start' and elem.tag == 'Runs':
          status = 4
        event, elem = next(context)

      elif status == 1:
        if event == 'start' and elem.tag == 'Summary':
          status = 2
        if event == 'end' and elem.tag == 'Experiment':
          self.docsums[uid].srx.acc = elem.attrib.pop('acc', None)
          self.docsums[uid].srx.name = elem.attrib.pop('name', None)
          self.docsums[uid].srx.status = elem.attrib.pop('status', None)
        if event == 'end' and elem.tag == 'Submitter':
          self.docsums[uid].srx.submitter.acc = elem.attrib.pop('acc', None)
          self.docsums[uid].srx.submitter.center = elem.attrib.pop('center_name', None)
          self.docsums[uid].srx.submitter.contact = elem.attrib.pop('contact_name', None)
          self.docsums[uid].srx.submitter.lab = elem.attrib.pop('lab_name', None)
        if event == 'end' and elem.tag == 'Study':
          self.docsums[uid].srp.acc = elem.attrib.pop('acc', None)
          self.docsums[uid].srp.name = elem.attrib.pop('name', None)
        if event == 'end' and elem.tag == 'Organism':
          self.docsums[uid].srs.taxid = elem.attrib.pop('taxid', None)
          self.docsums[uid].srs.organism = elem.attrib.pop('ScientificName=', None)
        if event == 'end' and elem.tag == 'Sample':
          self.docsums[uid].srs.acc = elem.attrib.pop('acc', None)
          self.docsums[uid].srs.name = elem.attrib.pop('name=', None)
        if event == 'start' and elem.tag == 'Library_descriptor':
          status = 3
        if event == 'end' and elem.tag == 'Bioproject':
          self.docsums[uid].srx.bioproject = elem.text
        if event == 'end' and elem.tag == 'Biosample':
          self.docsums[uid].srx.biosample = elem.text
        if event == 'end' and elem.tag == 'ExpXml':
          status = 0
        event, elem = next(context)

      elif status == 2:
        if event == 'end' and elem.tag == 'Platform':
          self.docsums[uid].srx.library.platform.name = elem.text
          self.docsums[uid].srx.library.platform.model = elem.attrib.pop('instrument_model', None)
        if event == 'end' and elem.tag == 'Statistics':
          self.docsums[uid].srx.spots = elem.attrib.pop('total_spots', 0)
          self.docsums[uid].srx.size = elem.attrib.pop('total_size', 0)
          self.docsums[uid].srx.bases = elem.attrib.pop('total_bases', 0)
        if event == 'end' and elem.tag == 'Summary':
          status = 1
        event, elem = next(context)

      elif status == 3:
        if event == 'end' and elem.tag == 'LIBRARY_NAME':
          self.docsums[uid].srx.library.name = elem.text
        if event == 'end' and elem.tag == 'LIBRARY_STRATEGY':
          self.docsums[uid].srx.library.strategy = elem.text
        if event == 'end' and elem.tag == 'LIBRARY_SOURCE':
          self.docsums[uid].srx.library.source = elem.text
        if event == 'end' and elem.tag == 'LIBRARY_SELECTION':
          self.docsums[uid].srx.library.selection = elem.text
        if event == 'start' and elem.tag == 'LIBRARY_LAYOUT':
          event, elem = next(context)
          self.parse_library_layout(self.docsums[uid], elem)
        if event == 'end' and elem.tag == 'LIBRARY_CONSTRUCTION_PROTOCOL':
          self.docsums[uid].srx.library.protocol = elem.text
        if event == 'end' and elem.tag == 'Library_descriptor':
          status = 1
        event, elem = next(context)

      elif status == 4: # Runs
        if event == 'end' and elem.tag == 'Run':
          srr = sralib.accstype.srr.Srr()
          srr.acc = elem.attrib.pop('acc', None)
          srr.spots = elem.attrib.pop('total_spots', None)
          srr.bases = elem.attrib.pop('total_bases', None)
          self.docsums[uid].srrs[srr.acc] = srr
        if event == 'end' and elem.tag == 'Runs':
          status = 0
        event, elem = next(context)

      else:
        status = 0
        event, elem = next(context)


  def parse_library_layout(self, docsum, elem):
    if elem.tag == 'SINGLE':
      docsum.srx.library.single = True
    if elem.tag == 'PAIRED':
      docsum.srx.library.paired = {'sdev': elem.attrib.pop('NOMINAL_SDEV', 0),
                                   'length' : elem.attrib.pop('NOMINAL_LENGTH', 0)}
