"""
..
  Copyright 2020 The University of Sydney
  SraRuninfoParser parses NCBI's SRA runinfo summaries [0] in XML format.
  It corresponds to the following EDirect pipeline: `esearch -db sra -query ERR2206317 | esummary -format runinfo -mode xml

  [0]: ## https://www.ncbi.nlm.nih.gov/books/NBK56913/
.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import os
import sys
import hashlib
import xml.etree.ElementTree

sys.path.insert(1, os.path.join(sys.path[0], '../include/blib/ncbi/src'))
import edirect.edbase.edanalyzer
import sra.sra_manager
import sra.accession_types.srastudy
import sra.accession_types.sraexperiment
import sra.accession_types.srarun
import sra.accession_types.srasample
import sra.srautils

class SraRuninfoParser(edirect.edbase.edanalyzer.EdAnalyzer):

  sramanager = sra.sra_manager.SraManager()

  def __init__(self):
    super().__init__()
    self.batch_runs = []

  def add_platform(self, platform):
    platform.create_uid()
    if platform.uid not in SraRuninfoParser.sramanager.platforms:
      SraRuninfoParser.sramanager.platforms[platform.uid] = platform
    return SraRuninfoParser.sramanager.platforms[platform.uid]

  def add_organism(self, organism):
    if organism.taxid not in SraRuninfoParser.sramanager.organisms:
      SraRuninfoParser.sramanager.organisms[organism.taxid] = organism
    return SraRuninfoParser.sramanager.organisms[organism.taxid]

  def add_bioproject(self, bioproject):
    if bioproject.accession not in SraRuninfoParser.sramanager.bioprojects:
      SraRuninfoParser.sramanager.bioprojects[bioproject.accession] = bioproject
    return SraRuninfoParser.sramanager.bioprojects[bioproject.accession]

  def analyze_result(self, request, srp=sra.accession_types.srastudy.SraStudy(),
                     srx=sra.accession_types.sraexperiment.SraExperiment(),
                     srr=sra.accession_types.srarun.SraRun(),
                     srs=sra.accession_types.srasample.SraSample()):

    for event, elem in xml.etree.ElementTree.iterparse(request.response, events=['start', 'end']):
      if elem.tag == 'Row':
        if event == 'start':
          srp = srp.new()
          srx = srx.new()
          srr = srr.new()
          srs = srs.new()
          platform = sra.srautils.SraPlatform()
          org = sra.srautils.SraOrganism()
          biop = sra.srautils.BioProject()
        if event == 'end':
          srp = SraRuninfoParser.sramanager.dedup(SraRuninfoParser.sramanager.studies, srp)
          srx = SraRuninfoParser.sramanager.dedup(SraRuninfoParser.sramanager.experiments, srx)
          srr = SraRuninfoParser.sramanager.dedup(SraRuninfoParser.sramanager.sraruns, srr)
          srs = SraRuninfoParser.sramanager.dedup(SraRuninfoParser.sramanager.samples, srs)
          srs.organism = self.add_organism(org)
          srp.link(srx, self.add_bioproject(biop))
          srx.link(srp, srr, srs, self.add_platform(platform))

      if event == 'end':
        if elem.tag == 'Run':
          srr.accession = elem.text
          self.batch_runs.append(srr.accession)
        if elem.tag == 'spots':
          srr.spots = int(elem.text)
        if elem.tag == 'spots_with_mates':
          srr.spots_with_mates = int(elem.text)
        if elem.tag == 'bases':
          srr.bases = int(elem.text)
        if elem.tag == 'avgLength':
          srr.avg_length = int(elem.text)
        if elem.tag == 'size_MB':
          srr.size_mb = int(elem.text)
        if elem.tag == 'download_path':
          srr.path = elem.text
        if elem.tag == 'Consent':
          srr.consent = elem.text
        if elem.tag == 'Experiment':
          srx.accession = elem.text
          srr.srx = elem.text
        if elem.tag == 'LibraryStrategy':
          srx.strategy = elem.text
        if elem.tag == 'LibrarySelection':
          srx.selection = elem.text
        if elem.tag == 'LibrarySource':
          srx.source = elem.text
        if elem.tag == 'LibraryLayout':
          srx.layout = elem.text
        if elem.tag == 'InsertSize':
          srx.insert_size = int(elem.text)
        if elem.tag == 'InsertDev':
          srx.insert_dev = float(elem.text)
        if elem.tag == 'Platform':
          platform.name = elem.text
        if elem.tag == 'Model':
          platform.model = elem.text
        if elem.tag == 'SRAStudy':
          srp.accession = elem.text
        if elem.tag == 'BioProject':
          biop.accession = elem.text
        if elem.tag == 'ProjectID':
          biop.uid = int(elem.text)
        if elem.tag == 'Sample':
          srs.accession = elem.text
        if elem.tag == 'BioSample':
          srs.accession = elem.text
        if elem.tag == 'SampleType':
          srs.type = elem.text
        if elem.tag == 'TaxID':
          org.taxid = int(elem.text)
        if elem.tag == 'ScientificName':
          org.name = elem.text
        if elem.tag == 'SampleName':
          srs.biosample = elem.text
        if elem.tag == 'Sex':
          srs.sex = elem.text


