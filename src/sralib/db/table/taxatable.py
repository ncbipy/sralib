"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import sqlite3
from typing import Iterable, Tuple, Type


import sralib.db.table.basetable

class TaxaTable(slms.db.table.basetable.BaseTable):

  def __init__(self, name:str, database:str):
    super().__init__(name, database)

  def create(self, connection:Type[sqlite3.Connection])->__qualname__:
    stmt = """CREATE TABLE IF NOT EXISTS taxid
              (id       INTEGER PRIMARY KEY,
               accs     TEXT NOT NULL, -- accession
               type     INT NOT NULL,  -- accession type (SRR, SRX, SRP..)
               taxid    INT NOT NULL,  -- Taxid
               UNIQUE(taxid, accs, type))"""
    connection.cursor().execute(stmt)
    self.create_index(connection, 'taxid',
                      'CREATE UNIQUE INDEX IF NOT EXISTS taxid ON taxid(taxid, accs, type)')
    return self

  def insert(self, connection:Type[sqlite3.Connection], values:Iterable[Tuple])->None:
    """Insert data into table"""
    raise NotImplementedError("Implement insert() method")

  def size(self):
    """ Get table size """
    c = self.database.execute_stmt("SELECT COUNT(id) FROM {0}".format(self.name))
    return c.fetchone()[0]
