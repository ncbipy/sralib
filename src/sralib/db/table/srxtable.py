"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import sqlite3
from typing import Iterable, Tuple, Type


import sralib.db.table.basetable

class SrxTable(slms.db.table.basetable.BaseTable):

  def __init__(self, name:str, database:str):
    super().__init__(name, database)

  def create(self, connection:Type[sqlite3.Connection])->__qualname__:
    stmt = """CREATE TABLE IF NOT EXISTS srx
              (id       INTEGER PRIMARY KEY,
               accs     TEXT NOT NULL,  -- SRX accession
               name     TEXT NOT NULL,  -- experiment name
               UNIQUE(accs))"""
    connection.cursor().execute(stmt)
    self.create_index(connection, 'srxaccs',
                      'CREATE UNIQUE INDEX IF NOT EXISTS srraccs ON srx(accs)')
    return self

  def insert(self, connection:Type[sqlite3.Connection], values:Iterable[Tuple])->None:
    """Insert data into table"""
    raise NotImplementedError("Implement insert() method")

  def size(self):
    """ Get table size """
    c = self.database.execute_stmt("SELECT COUNT(id) FROM {0}".format(self.name))
    return c.fetchone()[0]
