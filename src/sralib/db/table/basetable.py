"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import sqlite3
from typing import Iterable, Tuple, Type

class BaseTable:

  def __init__(self, name:str, database:str):
    """Initialize a new table"""
    self.name = name
    self.database = database
    self.indices = set()

  def create(self, connection:Type[sqlite3.Connection])->__qualname__:
    """Creating new table"""
    raise NotImplementedError("Implement create() method")

  def insert(self, connection:Type[sqlite3.Connection], values:Iterable[Tuple])->None:
    """Insert data into table"""
    raise NotImplementedError("Implement insert() method")

  def create_index(self, connection, idxname:str, stmt:str)->str:
    """Create indices for the table"""
    connection.cursor().execute(stmt)
    if idxname not in self.indices:
      self.indices.add(idxname)

  def size(self):
    """ Get table size """
    c = self.database.execute_stmt("SELECT COUNT(id) FROM {0}".format(self.name))
    return c.fetchone()[0]
