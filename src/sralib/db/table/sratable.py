"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import sqlite3
from typing import Iterable, Tuple, Type


import sralib.db.table.basetable

class SraTable(slms.db.table.basetable.BaseTable):

  def __init__(self, name:str, database:str):
    super().__init__(name, database)

  def create(self, connection:Type[sqlite3.Connection])->__qualname__:
    stmt = """CREATE TABLE IF NOT EXISTS sra
              (id       INTEGER PRIMARY KEY,
               accs     TEXT NOT NULL, -- SRA accession
               srp      TEXT NOT NULL, -- SRP accession
               srx      TEXT NOT NULL, -- SRX accession
               srr      TEXT NOT NULL, -- SRP accession
               srs      TEXT NOT NULL, -- SRP accession
               UNIQUE(accs))"""
    connection.cursor().execute(stmt)
    self.create_index(connection, 'srraccs',
                      'CREATE UNIQUE INDEX IF NOT EXISTS srraccs ON srr(accs)')
    return self

  def insert(self, connection:Type[sqlite3.Connection], values:Iterable[Tuple])->None:
    """Insert data into table"""
    raise NotImplementedError("Implement insert() method")

  def create_index(self, connection, idxname:str, stmt:str)->str:
    """Create indices for the table"""
    connection.cursor().execute(stmt)
    if idxname not in self.indices:
      self.indices.add(idxname)

  def size(self):
    """ Get table size """
    c = self.database.execute_stmt("SELECT COUNT(id) FROM {0}".format(self.name))
    return c.fetchone()[0]
