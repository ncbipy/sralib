"""
..
  Copyright 2020 The University of Sydney

.. moduleauthor:: Jan P Buchmann <jan.buchmann@sydney.edu.au>
"""

import sys
import logging
import sqlite3
from typing import Iterable, Dict, List, Tuple, Type


from sralib.db.table import srptable
from sralib.db.table import srxtable
from sralib.db.table import srrtable
from sralib.db.table import srstable
from sralib.db.table import srxrtable
from sralib.db.table import srsxtable
from sralib.db.table import taxatable

class MetadataDbManager:

  def __init__(self, dbpath:str):
    #self.logger = logging.getLogger(utils.resolve_log_nspace(TaxonomyDb))
    self.path = dbpath
    #self.logger.debug("{}: Create instance".format(self.path))
    self.connection = self.init_connection()
    self.srp = srptable.SrpTable(self.path).create(self.connection)
    self.srx = srxtable.SrxTable(self.path).create(self.connection)
    self.srr = srrtable.SrrTable(self.path).create(self.connection)
    self.srs = srstable.SrsTable(self.path).create(self.connection)
    self.srx_r = srxrtable.SrxSrrTable(self.path).create(self.connection)
    self.srs_x = srsxtable.SrsSrxTable(self.path).create(self.connection)
    self.taxa = taxatable.TaxaTable(self.path).create(self.connection)
    #self.logger.debug("{}: Database initialized".format(self.path))

  def init_connection(self)->sqlite3.Connection:
    #self.logger.debug("{}: Connecting".format(self.path))
    connection = sqlite3.connect(self.path)
    connection.execute("PRAGMA synchronous=NORMAL")
    connection.execute("PRAGMA journal_mode=WAL")
    connection.execute("PRAGMA foreign_keys=1")
    connection.row_factory = sqlite3.Row
    #self.logger.debug("{}: Connected".format(self.path))
    return connection

  def close_connection(self)->None:
    #self.logger.debug("{}: Closing connection".format(self.path))
    self.connection.close()

  def connect(self)->sqlite3.Connection:
    if self.connection is None:
      return self.init_connection(self.path)
    return self.connection

